import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.sql.Date.*;

public class BlockSites {
	public static void main(String[] args) throws IOException, InterruptedException{
		String hostFileName = "C:\\Windows\\System32\\drivers\\etc\\hosts";
		try(BufferedReader in = JavaUtil.getReader(hostFileName)){
			blockOrUnblockSiteAtMethod(hostFileName, new Date(), 14, 41, 00, "www.google.com", true);
			blockOrUnblockSiteAtMethod(hostFileName, new Date(), 14, 41, 45, "www.google.com", false);
		}
		catch(FileNotFoundException fnfe){
			System.out.println("Invalid File - File doesn't exist.. "+fnfe);
		}
	}
	
	public static void blockOrUnblockSiteAtMethod(String hostFileName, Date currentDate, int hours, int minutes, int seconds, String domainName, boolean block) 
												throws IOException, InterruptedException{
		String line = null;
		String localHost = "127.0.0.1";	
		BufferedWriter out = null;
		BufferedReader in = null;
		String blockOrUnblock = null;
		
		if(block == true)
			blockOrUnblock = "Block";
		else
			blockOrUnblock = "Unblock";
			
		//giving all permissions to 'hosts' file
		JavaUtility.addAllPermissionsToFileInWindows(hostFileName);
		
		//setting block/unblock start time
		Date setBlockOrUnblockTime = new Date();
		if(currentDate.getHours()>hours){
			setBlockOrUnblockTime.setDate(setBlockOrUnblockTime.getDate()+1);
		}// else IllegalArgumentException in sleep() - Timeout value is negative
		setBlockOrUnblockTime.setHours(hours);
		setBlockOrUnblockTime.setMinutes(minutes);
		setBlockOrUnblockTime.setSeconds(seconds);
		
		//sleeping till the block/unblock time starts
		long setBlockOrUnblockTimeInSec = setBlockOrUnblockTime.getTime();
		long currTimeInSec = currentDate.getTime();
		System.out.println("\n\nCurrent Time:\n\t"+currentDate+"\n"+blockOrUnblock+"ing at:\n\t"+setBlockOrUnblockTime+"\nPlease wait...!");
		java.lang.Thread.sleep(setBlockOrUnblockTimeInSec-currTimeInSec);
		
		//once unblock/block time starts, edit the hosts file to allow/disallow browsing the given domainName
		String copiedHostsFile = "D:\\copiedHostsFile";
		JavaUtility.addAllPermissionsToFileInWindows(copiedHostsFile);
		boolean domainNameAlreadyPresent = false;
	    in = JavaUtil.getReader(hostFileName);
		out = new BufferedWriter(new FileWriter(copiedHostsFile));
		if(block == true){
			//if domainName is already registered, just block it
			while((line = in.readLine())!=null){
				if(line.contains(domainName) == true){
					domainNameAlreadyPresent = true;
					line = line.substring(1,line.length());
					//System.out.println("Line: "+line);
				}
				out.write(line+"\n");
			}
			//if domainName is not registered, add it to block it
			if(domainNameAlreadyPresent == false){
				out.append(localHost+"\t"+domainName+"\n");
			}
		}
		else {
			//unblock the given domainName
			while((line = in.readLine())!=null){
				if(line.contains(domainName)==true){
					line = "#"+line;
					//System.out.println("Line: "+line);
				}
				out.write(line+"\n");
			}
		}
		in.close();
		out.close();
		//Copying the content in hosts file.
		//Files.copy(Paths.get(copiedHostsFile), Paths.get(hostFileName), StandardCopyOption.REPLACE_EXISTING);// - System files -- AccesDeniedException
		out = new BufferedWriter(new FileWriter(hostFileName));
		in = new BufferedReader(new FileReader(copiedHostsFile));
		while((line = in.readLine())!=null){
			out.write(line+"\n");
		}
		in.close();
		out.close();
		//Deleting the temp file
		Files.delete(Paths.get(copiedHostsFile));
		if(block ==  true)
			System.out.println("\n\n******************  Sorry! Blocked! :(  ******************\n\n");
		else
			System.out.println("\n\n*****************  Hurray! Unblocked! :D  ****************\n\n");
	}
}

