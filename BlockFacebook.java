import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.sql.Date.*;

public class BlockFacebook {
	public static void main(String[] args) throws IOException, InterruptedException{
		String hostFileName = "C:\\Windows\\System32\\drivers\\etc\\hosts";
		//BufferedReader in = null;
		try(BufferedReader in = JavaUtil.getReader(hostFileName)){
			//Current Time
			Date currentDate = new Date();
			//unblockSiteAtMethod(hostFileName, currentDate, hours, domainName);
			unblockSiteAtMethod(hostFileName, currentDate, 20, 40);
			//blockSiteAtMethod(hostFileName, currentDate, unblockAfterTheseManyHours, domainName);
			blockSiteAtMethod(hostFileName, currentDate, 20, 45);
		}
		catch(FileNotFoundException fnfe){
			System.out.println("Invalid File - File doesn't exist");
		}
		/*finally{
			if(in!=null)
				in.close();
		} */ //not required for 'in', as we are using try-with-resource -  BufferedReader implements java.lang.AutoClosable interface and hence close operation is performed automatically 
	}
	
	public static void unblockSiteAtMethod(String hostFileName, Date currentDate, int hours, int minutes) throws IOException, InterruptedException{
		String line = null;
		String[] arr = null;	
		BufferedWriter out = null;
		BufferedReader in = JavaUtil.getReader(hostFileName);
		//unblock start time = 13:00
		Date setUnblockStartTime = new Date();
		//int hours = setUnblockStartTime.getHours();
		System.out.println("...."+setUnblockStartTime.getHours());
		/*if(hours>13){
			setUnblockStartTime.setDate(setUnblockStartTime.getDate()+1);
		}*/
		setUnblockStartTime.setHours(hours);
		setUnblockStartTime.setMinutes(minutes);
		setUnblockStartTime.setSeconds(00);
		java.sql.Date sqlsetUnblockStartTime = JavaUtil.convertUtilDateToSqlDate(setUnblockStartTime);
		//Sleep till unblock time starts - 13:00
		long setUnblockStartTimeInSec = setUnblockStartTime.getTime();
		long currTimeInSec = currentDate.getTime();
		System.out.println("CurrentTime: "+currentDate.getTime()+"\nUnblocking in..."+setUnblockStartTime.getTime()+"...milliseconds! :)");
		java.lang.Thread.sleep(setUnblockStartTimeInSec-currTimeInSec);
		//once unblock time starts, edit the hosts file to allow browsing Facebook
		String unblockedFileName = "D:\\hostsUnblocked";
		out = new BufferedWriter(new FileWriter(unblockedFileName));
		while((line = in.readLine())!=null){
			if(line.contains("www.facebook.com")==true){
				line = line.substring(1,line.length());
				System.out.println("Line: "+line);
			}
			out.write(line+"\n");
		}
		in.close();
		out.close();
		//Copying the content in hosts file.
		//Files.copy(Paths.get(unblockedFileName), Paths.get(hostFileName), StandardCopyOption.REPLACE_EXISTING); - System files -- AccesDeniedException
		out = new BufferedWriter(new FileWriter(hostFileName));
		in = new BufferedReader(new FileReader(unblockedFileName));
		while((line = in.readLine())!=null){
			out.write(line+"\n");
		}
		in.close();
		out.close();
		//Deleting the temp file
		//Files.delete(Paths.get(unblockedFileName));
		System.out.println("Hurray! Unblocked! :D ");
	}
	
	public static void blockSiteAtMethod(String hostFileName, Date currentDate, int unblockAfterAtHours, int minutes) throws IOException, InterruptedException{
		String line = null;
		String[] arr = null;	
		BufferedWriter out = null;
		BufferedReader in = JavaUtil.getReader(hostFileName);
		//unblock end time or block start time
		Date setUnblockEndTime = new Date();
		setUnblockEndTime.setHours(unblockAfterAtHours);
		setUnblockEndTime.setMinutes(minutes);
		setUnblockEndTime.setSeconds(00);
		System.out.println("CurrentTime: "+currentDate.getTime()+"\nBlocking in..."+setUnblockEndTime.getTime()+"...milliseconds! :)");
		//Sleep till unblock time ends - 14:00
		long setUnblockEndTimeInSec = setUnblockEndTime.getTime();
		long currTimeInSec = currentDate.getTime();
		java.lang.Thread.sleep(setUnblockEndTimeInSec-currTimeInSec);
		//once unblock time ends, edit the hosts file to disallow browsing Facebook
		String blockedFileName = "D:\\hostsBlocked";
		out = new BufferedWriter(new FileWriter(blockedFileName));
		in = new BufferedReader(new FileReader(hostFileName));
		while((line = in.readLine())!=null){
			if(line.contains("www.facebook.com")==true){
				line = "#"+line;
				System.out.println("Line: "+line);
			}
			out.write(line+"\n");
		}
		in.close();
		out.close();
		//Copying the content in hosts file.
		//Files.copy(Paths.get(blockedFileName), Paths.get(hostFileName), StandardCopyOption.REPLACE_EXISTING); - System files -- AccesDeniedException
		out = new BufferedWriter(new FileWriter(hostFileName));
		in = new BufferedReader(new FileReader(blockedFileName));
		while((line = in.readLine())!=null){
			out.write(line+"\n");
		}
		in.close();
		out.close();
		//Deleting the temp file
		//Files.delete(Paths.get(blockedFileName));
		System.out.println("Sorry! Blocked! :( ");
	}
}

class JavaUtil{
	public static BufferedReader getReader(String fileName) throws IOException{
		return new BufferedReader(new FileReader(fileName));
	}
	public static java.sql.Date convertUtilDateToSqlDate(java.util.Date date){
		java.sql.Date  sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
}
