import java.nio.file.*;
import java.io.*;
import java.util.*;
import java.nio.file.attribute.PosixFilePermission;

public class JavaUtility{
	public static BufferedReader getReader(String fileName) throws IOException{
		return new BufferedReader(new FileReader(fileName));
	}
	public static java.sql.Date convertUtilDateToSqlDate(java.util.Date date){
		java.sql.Date  sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
	public static void addAllPermissionsToFileInLinux(String fileName) throws IOException{ //Windows doesn't support Posix 
		Set<PosixFilePermission> filePermissionParams =  new HashSet<PosixFilePermission>();
		//add owners permission
        filePermissionParams.add(PosixFilePermission.OWNER_READ);
        filePermissionParams.add(PosixFilePermission.OWNER_WRITE);
        filePermissionParams.add(PosixFilePermission.OWNER_EXECUTE);
        //add group permissions
        filePermissionParams.add(PosixFilePermission.GROUP_READ);
        filePermissionParams.add(PosixFilePermission.GROUP_WRITE);
        filePermissionParams.add(PosixFilePermission.GROUP_EXECUTE);
        //add others permissions
        filePermissionParams.add(PosixFilePermission.OTHERS_READ);
        filePermissionParams.add(PosixFilePermission.OTHERS_WRITE);
        filePermissionParams.add(PosixFilePermission.OTHERS_EXECUTE);
		Files.setPosixFilePermissions(Paths.get(fileName), filePermissionParams);//returns Path
	}
	public static void addAllPermissionsToFileInWindows(String fileName) throws IOException{
		File file = new File(fileName); 
        //set permissions to Owner
        file.setExecutable(true);
        file.setReadable(true);
        file.setWritable(true);
		//setExecutable(boolean executable, boolean ownerOnly) - change permission to 777 for all the users
        file.setExecutable(true, false);
        file.setReadable(true, false);
        file.setWritable(true, false);
		//System.out.println("IsWritable?: "+file.canWrite());
	}
}